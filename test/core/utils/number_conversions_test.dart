import 'package:flutter_test/flutter_test.dart';

import 'package:greentick/utils/number_conversions.dart';

void main() {
  test('number should be converted to 220 K on input of 220000', () {
    num value = 220000;
    var convertedNumber = NumberConversions.numberFormatter(value);
    expect(convertedNumber, '220 K');
  });

  test('number should be converted to 12.0 M on input of 12000000', () {
    num value = 12000000;
    var convertedNumber = NumberConversions.numberFormatter(value);
    expect(convertedNumber, '12.0 M');
  });

  test('number should be not be converted if less than 1000', () {
    num value = 120;
    var convertedNumber = NumberConversions.numberFormatter(value);
    expect(convertedNumber, '120');
  });
}
