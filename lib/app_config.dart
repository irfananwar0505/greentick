import 'package:flutter/material.dart';

/// [AppConfig] is a configuration class which can be user
/// to add any environment related configurations
class AppConfig extends InheritedWidget {
  final String apiBaseUrl;
  @override
  // ignore: overridden_fields
  final Widget child;

  const AppConfig({
    Key? key,
    required this.apiBaseUrl,
    required this.child,
  }) : super(
          key: key,
          child: child,
        );

  static AppConfig? of(BuildContext? context) {
    return context?.dependOnInheritedWidgetOfExactType(aspect: AppConfig);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
