import 'package:flutter/material.dart';

import 'package:greentick/presentation/widgets/search/search_widget.dart';

/// [SearchScreen] is a screen level file responsible to hold all containers
/// for displaying retail shop details
class SearchScreen extends StatelessWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: SingleChildScrollView(
        child: SearchWidget(),
      ),
    );
  }
}
