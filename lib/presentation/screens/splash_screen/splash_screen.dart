import 'package:flutter/material.dart';

import 'package:greentick/resources/strings/strings.dart';
import 'package:greentick/themes/colors.dart';
import 'package:greentick/themes/global_text_theme.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Color?> _colorAnimation;
  late Animation<double> _sizeAnimation;
  late Animation<double> _sizeAnimation2;
  late Animation<double> _curve;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 2500),
      vsync: this,
    );
    _curve =
        CurvedAnimation(parent: _animationController, curve: Curves.bounceOut);
    _colorAnimation =
        ColorTween(begin: AppColors.lightGreen, end: AppColors.funGreen)
            .animate(_curve);
    _sizeAnimation = Tween<double>(begin: 0, end: 32).animate(_curve);
    _sizeAnimation2 = Tween<double>(begin: 16, end: 12).animate(_curve);
    _animationController.forward();
    _animationController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        color: _colorAnimation.value,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Text(
                  Strings.greenTick,
                  style: Theme.of(context)
                      .textTheme
                      .whiteHeadLine1!
                      .copyWith(fontSize: _sizeAnimation.value),
                ),
                Text(
                  Strings.appBasedPlatform,
                  style: Theme.of(context)
                      .textTheme
                      .whiteHeadline6!
                      .copyWith(fontSize: _sizeAnimation2.value),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
