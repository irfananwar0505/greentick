import 'package:flutter/material.dart';

import 'package:greentick/containers/retail_shop/retail_shop_container.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: RetailShopContainer(),
    );
  }
}
