import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:greentick/presentation/screens/splash_screen/splash_screen.dart';

import 'package:greentick/routes/routes.dart';

/// [InitialScreen] is resposible for executing whenver app is opened
/// and redirecting user to specific flows
class InitialScreen extends StatefulWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  State<InitialScreen> createState() => _InitialScreenState();
}

class _InitialScreenState extends State<InitialScreen> {
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      Future.delayed(const Duration(milliseconds: 3000), () {
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.dashboardScreen, (route) => false);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return const SplashScreen();
  }
}
