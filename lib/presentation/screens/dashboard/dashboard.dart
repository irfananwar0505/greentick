import 'package:flutter/material.dart';

import 'package:greentick/presentation/screens/common/bottom_nav_layout.dart';
import 'package:greentick/presentation/screens/home/home.dart';
import 'package:greentick/presentation/screens/search/search_screen.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const BottomNavLayout(
      pageWidgets: [
        Home(),
        SearchScreen(),
      ],
    );
  }
}
