import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:greentick/presentation/widgets/common/app_drawer.dart';
import 'package:greentick/presentation/widgets/common/bottom_nav.dart';
import 'package:greentick/themes/colors.dart';

///[BottomNavLayout] is used for the layout using bottom nav

class BottomNavLayout extends StatefulWidget {
  final List<Widget> pageWidgets;

  const BottomNavLayout({
    Key? key,
    required this.pageWidgets,
  }) : super(key: key);

  @override
  State<BottomNavLayout> createState() => _BottomNavLayoutState();
}

class _BottomNavLayoutState extends State<BottomNavLayout> {
  int selectedIndex = 0;
  void onItemTapped(index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.lightGreen,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            Icon(Icons.location_pin),
            Text('Chalad, Kannur'),
          ],
        ),
        actions: [
          Padding(
            padding: EdgeInsets.all(8.0.r),
            child: const Icon(
              Icons.check_circle_outline_rounded,
              color: AppColors.jewel,
            ),
          )
        ],
      ),
      body: widget.pageWidgets.elementAt(selectedIndex),
      bottomNavigationBar: SizedBox(
        height: 100.0.h,
        child: BottomNavBar(
          selectedIndex: selectedIndex,
          onItemTapped: onItemTapped,
        ),
      ),
      drawer: const AppDrawer(),
    );
  }
}
