import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:greentick/resources/strings/strings.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(8.0.r),
          child: TextField(
            style: const TextStyle(
              fontSize: 25.0,
              color: Colors.blueAccent,
            ),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(
                vertical: 20.h,
                horizontal: 15.w,
              ),
              prefixIcon: const Icon(Icons.search),
              hintText: Strings.searchText,
            ),
          ),
        ),
      ],
    );
  }
}
