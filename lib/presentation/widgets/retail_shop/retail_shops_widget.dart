import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:greentick/core/models/retail_shop_model.dart';
import 'package:greentick/presentation/widgets/retail_shop/retail_shop_card.dart';
import 'package:greentick/resources/images/images.dart';
import 'package:greentick/resources/strings/strings.dart';
import 'package:greentick/themes/colors.dart';
import 'package:greentick/themes/global_text_theme.dart';

/// [RetailShopsWidget] is a pure UI widget which is used to display the retail shops data
class RetailShopsWidget extends StatelessWidget {
  final List<RetailShopModel> retailShops;
  const RetailShopsWidget({
    required this.retailShops,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(8.0.w),
          child: Text(
            Strings.bookYourTravel,
            style: Theme.of(context).textTheme.headline4,
          ),
        ),
        Image.asset(Images.retailHeader),
        SizedBox(
          height: 10.h,
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 10.h),
          height: 240.h,
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(
                color: AppColors.downy,
                width: 3.0,
              ),
              bottom: BorderSide(
                color: AppColors.downy,
                width: 3.0,
              ),
            ),
          ),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(12.0.r),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.thumb_up,
                          color: AppColors.matrixLight,
                          size: 20.r,
                        ),
                        SizedBox(
                          width: 5.r,
                        ),
                        Text(
                          Strings.recommendedForYou,
                          style: Theme.of(context).textTheme.subHeader,
                        ),
                      ],
                    ),
                    Text(
                      Strings.viewAll,
                      style: Theme.of(context).textTheme.subHeader2,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 180.h,
                child: ListView.builder(
                  itemCount: retailShops.length,
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.symmetric(horizontal: 5.w),
                  itemBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      width: 240.w,
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 5.w),
                        child: RetailShopCard(
                          imageUrl: retailShops[index].imageUrl,
                          name: retailShops[index].name,
                          dislikes: retailShops[index].dislikes,
                          likes: retailShops[index].likes,
                          rating: retailShops[index].rating,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
