import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:greentick/presentation/widgets/common/cached_network_image.dart';
import 'package:greentick/presentation/widgets/common/star_rating.dart';
import 'package:greentick/resources/images/images.dart';
import 'package:greentick/themes/colors.dart';
import 'package:greentick/themes/global_text_theme.dart';
import 'package:greentick/utils/number_conversions.dart';

/// [RetailShopCard] displays individual retail shops details
class RetailShopCard extends StatelessWidget {
  final String imageUrl, name;
  final num likes, dislikes, rating;
  const RetailShopCard({
    Key? key,
    required this.imageUrl,
    required this.name,
    this.likes = 0,
    this.dislikes = 0,
    this.rating = 0,
  }) : super(key: key);

  // [defaultFeedImage] displays the group and user default image
  Widget defaultImage() {
    return Image.asset(
      Images.placeHolder,
      fit: BoxFit.fill,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      color: AppColors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
            color: AppColors.transparent,
            elevation: 5,
            child: CachedNetworkImageWidget(
              cacheKey: '1',
              stalePeriod: const Duration(days: 1),
              imageUrl: imageUrl,
              placeHolder: defaultImage(),
              errorWidget: defaultImage(),
              height: 100.h,
              width: 250.w,
              fit: BoxFit.fill,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0.r),
            child: Column(
              children: [
                Text(
                  name,
                  style: Theme.of(context).textTheme.secondaryHeadline6,
                ),
                SizedBox(
                  height: 8.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.thumb_up,
                      color: AppColors.funGreen,
                      size: 12.r,
                    ),
                    const SizedBox(
                      width: 2,
                    ),
                    Text(
                      NumberConversions.numberFormatter(likes),
                      style: Theme.of(context).textTheme.numberText,
                    ),
                    SizedBox(
                      width: 8.w,
                    ),
                    Icon(
                      Icons.thumb_down,
                      color: AppColors.matrixLight,
                      size: 12.r,
                    ),
                    const SizedBox(
                      width: 2,
                    ),
                    Text(
                      NumberConversions.numberFormatter(dislikes),
                      style: Theme.of(context).textTheme.numberText,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    StarRating(rating: rating)
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
