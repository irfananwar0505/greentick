import 'package:flutter/material.dart';

import 'package:greentick/resources/strings/strings.dart';
import 'package:greentick/themes/colors.dart';

///  [BottomNavBar] is the common footer nav which will be used through out the app.
///  This includes all the tabs which is required for the app.
///
class BottomNavBar extends StatelessWidget {
  final int selectedIndex;
  final void Function(int) onItemTapped;

  const BottomNavBar({
    Key? key,
    required this.selectedIndex,
    required this.onItemTapped,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topRight: Radius.circular(40),
        topLeft: Radius.circular(40),
      ),
      child: BottomNavigationBar(
        currentIndex: selectedIndex,
        onTap: onItemTapped,
        backgroundColor: AppColors.lightGreen,
        selectedItemColor: AppColors.white,
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_filled),
            label: Strings.home,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: Strings.search,
          ),
        ],
      ),
    );
  }
}
