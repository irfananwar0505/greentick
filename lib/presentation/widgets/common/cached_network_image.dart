import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

/// [CachedNetworkImageWidget] is a common Image widget which
/// caches the current image url and displays it until the stale period is over
class CachedNetworkImageWidget extends StatelessWidget {
  final String cacheKey;
  final Duration stalePeriod;
  final Widget placeHolder;
  final Widget errorWidget;
  final String imageUrl;
  final BoxFit fit;
  final double height;
  final double width;
  const CachedNetworkImageWidget({
    Key? key,
    required this.cacheKey,
    required this.stalePeriod,
    required this.placeHolder,
    required this.errorWidget,
    required this.imageUrl,
    required this.fit,
    required this.height,
    required this.width,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(
            Radius.circular(10.0.r),
          ),
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
      ),
      height: height,
      width: width,
      cacheManager: CacheManager(
        Config(
          cacheKey,
          stalePeriod: stalePeriod,
        ),
      ),
      placeholder: (BuildContext buildContext, _) => placeHolder,
      errorWidget: (BuildContext builContext, _arg, _arg1) => errorWidget,
      imageUrl: imageUrl,
      fit: fit,
    );
  }
}
