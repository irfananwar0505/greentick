import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:greentick/resources/images/images.dart';
import 'package:greentick/themes/colors.dart';
import 'package:greentick/utils/greetings.dart';

/// [AppDrawer] holds the app drawer part of layout
class AppDrawer extends StatelessWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(
              color: AppColors.lightGreen,
            ),
            child: Column(
              children: [
                const CircleAvatar(
                  backgroundImage: AssetImage(
                    Images.userImage,
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Text('${Greetings.greeting()}, Irfan Anwar'),
              ],
            ),
          ),
          ListTile(
            title: const Text('Privacy Policy'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Terms and Conditions'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('F.A.Q'),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Logout'),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
