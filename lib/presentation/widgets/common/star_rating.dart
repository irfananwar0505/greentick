import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:greentick/themes/colors.dart';
import 'package:greentick/themes/global_text_theme.dart';

/// [StarRating] is a common widget which can be used to display
/// the rating of any product/shops
class StarRating extends StatelessWidget {
  final num rating;
  const StarRating({
    Key? key,
    required this.rating,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: List.generate(
            5,
            (index) {
              return index > rating - 1 && index < rating
                  ? Icon(
                      Icons.star_half,
                      size: 10.r,
                      color: AppColors.jewel,
                    )
                  : Icon(
                      index < rating ? Icons.star : Icons.star_border,
                      size: 10.r,
                      color: AppColors.jewel,
                    );
            },
          ),
        ),
        const SizedBox(
          width: 2,
        ),
        Text(
          rating.toString(),
          style: Theme.of(context).textTheme.numberText,
        ),
      ],
    );
  }
}
