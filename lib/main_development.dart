import 'package:flutter/material.dart';

import 'app.dart';
import 'app_config.dart';

Widget getConfigurations(Widget mainApp) {
  return AppConfig(
    apiBaseUrl: 'https://mocki.io/v1/',
    child: mainApp,
  );
}

Future main() async {
  var configuredApp = getConfigurations(const App());

  runApp(configuredApp);
}
