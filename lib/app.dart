import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:greentick/routes/routes.dart';
import 'package:greentick/themes/theme.dart';

class App extends StatelessWidget {
  const App({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(414, 896),
      builder: () => MaterialApp(
        theme: AppTheme.buildLightTheme(),
        debugShowCheckedModeBanner: false,
        onGenerateRoute: Routes.onGenerateRoute,
      ),
    );
  }
}
