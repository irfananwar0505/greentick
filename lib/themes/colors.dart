import 'package:flutter/material.dart';

/// Define all the colors to be used in application in this file
/// To use - import this file and call required string by:
///```dart
///      AppColors.<name>
///```
class AppColors {
  AppColors._();
  static const Color scaffoldBackground = Color.fromRGBO(250, 250, 250, 1);
  static const Color black = Color.fromRGBO(0, 0, 0, 1);
  static const Color white = Color.fromRGBO(255, 255, 255, 1);
  static const Color como = Color.fromRGBO(79, 139, 108, 1);
  static const Color silverChalice = Color.fromRGBO(168, 168, 168, 1);
  static const Color jewel = Color.fromRGBO(23, 114, 63, 1);
  static const Color matrix = Color.fromRGBO(170, 86, 86, 1);
  static const Color pharlap = Color.fromRGBO(166, 119, 122, 1);
  static const Color matrixLight = Color.fromRGBO(168, 86, 81, 1);
  static const Color funGreen = Color.fromRGBO(0, 125, 51, 1);
  static const Color downy = Color.fromRGBO(89, 209, 183, 1);
  static const Color transparent = Colors.transparent;
  static const Color lightGreen = Colors.lightGreen;
  static const Color error = Colors.red;
}
