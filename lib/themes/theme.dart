import 'package:flutter/material.dart';

import 'colors.dart';

class AppTheme {
  AppTheme._();
  static final ThemeData baseTheme = ThemeData.light();
  static ThemeData buildLightTheme() {
    return ThemeData(
      fontFamily: 'Roboto',
      scaffoldBackgroundColor: AppColors.scaffoldBackground,
      primaryColor: AppColors.white,
      textTheme: _buildTextTheme(),
      errorColor: AppColors.error,
      colorScheme:
          ColorScheme.fromSwatch().copyWith(secondary: AppColors.white),
    );
  }

  static TextTheme _buildTextTheme() {
    return baseTheme.textTheme
        .copyWith(
          headline1: baseTheme.textTheme.headline1?.copyWith(
            color: AppColors.black,
            fontSize: 32,
            fontWeight: FontWeight.w500,
          ),
          headline2: baseTheme.textTheme.headline2?.copyWith(
            color: AppColors.black,
            fontSize: 24,
            fontWeight: FontWeight.w500,
          ),
          headline3: baseTheme.textTheme.headline3?.copyWith(
            color: AppColors.black,
            fontSize: 24,
            fontWeight: FontWeight.w400,
          ),
          headline4: baseTheme.textTheme.headline4?.copyWith(
            color: AppColors.black,
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
          headline5: baseTheme.textTheme.headline5?.copyWith(
            color: AppColors.black,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
          headline6: baseTheme.textTheme.headline6?.copyWith(
            color: AppColors.black,
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
          subtitle1: baseTheme.textTheme.subtitle1?.copyWith(
            color: AppColors.black,
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
          subtitle2: baseTheme.textTheme.subtitle2?.copyWith(
            color: AppColors.black,
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),
          bodyText1: baseTheme.textTheme.bodyText1?.copyWith(
            color: AppColors.black,
            fontSize: 12,
            fontWeight: FontWeight.w500,
          ),
          bodyText2: baseTheme.textTheme.bodyText2?.copyWith(
            color: AppColors.black,
            fontSize: 12,
            fontWeight: FontWeight.w400,
          ),
          button: baseTheme.textTheme.button?.copyWith(
            color: AppColors.white,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
        )
        .apply(
          fontFamily: 'Roboto',
        );
  }
}
