import 'package:flutter/material.dart';

import 'package:greentick/themes/colors.dart';

extension ThemeTextExtension on TextTheme {
  TextStyle? get secondaryHeadline6 =>
      headline6?.copyWith(color: AppColors.como);
  TextStyle? get whiteHeadline6 => headline6?.copyWith(color: AppColors.white);
  TextStyle? get whiteHeadLine1 => headline1?.copyWith(color: AppColors.white);
  TextStyle? get numberText =>
      bodyText2?.copyWith(color: AppColors.silverChalice);
  TextStyle? get subHeader => headline6?.copyWith(color: AppColors.matrix);
  TextStyle? get subHeader2 => bodyText1?.copyWith(color: AppColors.pharlap);
}
