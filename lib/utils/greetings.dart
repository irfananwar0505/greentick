import 'package:greentick/resources/strings/strings.dart';

class Greetings {
  static String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return Strings.goodMorning;
    }
    if (hour < 17) {
      return Strings.goodAfternoon;
    }
    return Strings.goodEvening;
  }
}
