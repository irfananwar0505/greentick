import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:greentick/core/models/retail_shop_model.dart';
import 'package:http/http.dart' as http;

/// [RetailShopServices] any service calls related to retail shops goes here
class RetailShopServices {
  /// [getRetailShopList] returns the list of retail shops data
  static Future<List<RetailShopModel>> getRetailShopList(BuildContext context,
      {String baseApiUrl = ''}) async {
    // var response = await DefaultAssetBundle.of(context)
    //     .loadString("mock/retail_shops.json");
    var response = await http
        .get(Uri.parse('${baseApiUrl}bf8911d7-6948-41fe-a5c8-fb27fd130baa'));
    List data = jsonDecode(response.body);
    List<RetailShopModel> retailShops = [];
    for (var project in data) {
      retailShops.add(
        RetailShopModel.fromJson(project),
      );
    }
    return retailShops;
  }
}
