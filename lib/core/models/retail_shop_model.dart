/// [RetailShopModel] is the data model for displaying Retail Shop info on screen
class RetailShopModel {
  RetailShopModel({
    required this.name,
    required this.likes,
    required this.dislikes,
    required this.rating,
    required this.imageUrl,
  });

  String name, imageUrl;
  int likes;
  int dislikes;
  double rating;

  factory RetailShopModel.fromJson(Map<String, dynamic> json) =>
      RetailShopModel(
        name: json["name"] ?? '',
        imageUrl: json['imageUrl'] ?? '',
        likes: json["likes"] ?? 0,
        dislikes: json["dislikes"] ?? 0,
        rating: json["rating"]?.toDouble() ?? 0.0,
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "likes": likes,
        "dislikes": dislikes,
        "rating": rating,
        "imageUrl": imageUrl,
      };
}
