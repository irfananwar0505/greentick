import 'package:flutter/material.dart';

import 'package:greentick/app_config.dart';
import 'package:greentick/core/models/retail_shop_model.dart';
import 'package:greentick/core/services/retail_shop_services.dart';
import 'package:greentick/presentation/widgets/retail_shop/retail_shops_widget.dart';
import 'package:greentick/resources/strings/strings.dart';

/// [RetailShopContainer] holds the [RetailShopsWidget] to which the
/// required data will be fetched from the service files and passed
class RetailShopContainer extends StatelessWidget {
  const RetailShopContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<RetailShopModel>>(
      future: RetailShopServices.getRetailShopList(
        context,
        baseApiUrl: AppConfig.of(context)!.apiBaseUrl,
      ),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.hasData) {
          return RetailShopsWidget(
            retailShops: snapshot.data!,
          );
        }

        return const Center(child: Text(Strings.somethingWentWrong));
      },
    );
  }
}
