import 'package:flutter/material.dart';

import 'package:greentick/presentation/screens/dashboard/dashboard.dart';
import 'package:greentick/presentation/screens/initial/init.dart';

/// [Routes] which contains common routing configuaration needed for the Application.
/// This includes all pages and all routes mapped to each other.
/// This also includes Routing table and Route for Unhandled routes.
/// Use [Routes] in MaterialApp:

class Routes {
  Routes._();

  // create route constants
  // this will make navigation code handy that developers
  // will not have to remember the exact path
  static const initScreen = '/initial-screen';
  static const dashboardScreen = '/dashboard';

  static Route onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case initScreen:
        return MaterialPageRoute(builder: (_) => const InitialScreen());
      case dashboardScreen:
        return MaterialPageRoute(builder: (_) => const Dashboard());
      default:
        return MaterialPageRoute(builder: (_) => const InitialScreen());
    }
  }
}
