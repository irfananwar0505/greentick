/// Define all the images to be used in application in this file
/// To use - import this file and call required image by:
///```dart
///      Images.<name>
///```
class Images {
  Images._();

  static const String userImage = 'assets/images/profile_image.png';
  static const String placeHolder = 'assets/images/place_holder.png';
  static const String connectTogether = 'assets/images/connect_together.png';
  static const String retailHeader = 'assets/images/retail_header_image.png';
}
