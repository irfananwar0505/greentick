/// Define all the strings to be used in application in this file
/// To use - import this file and call required string by:
///```dart
///      Strings.<name>
///```
class Strings {
  Strings._();
  static const greenTick = 'Green Tick';
  static const connectingMicroCommunities =
      'Connecting the micro-communities together';
  static const appBasedPlatform = 'An App Based Platform for Micro-Communities';
  static const findYourRecommendation =
      'Find your recommendations in the RETAIL section';
  static const bookYourTravel = 'Book your travel';
  static const recommendedForYou = 'Recommended for you';
  static const viewAll = 'View All';
  static const home = 'Home';
  static const search = 'Search';
  static const searchText = 'Enter the product name';

  static const goodMorning = 'Good Morning';
  static const goodAfternoon = 'Good Afternoon';
  static const goodEvening = 'Good Evening';
  static const somethingWentWrong = 'Ooops !! Something went wrong';
}
